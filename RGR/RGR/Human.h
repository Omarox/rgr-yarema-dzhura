/***********************************************************************
* Module:  patient.h
* Author:  Yarema & Dzhura
* Modified: 19 ������� 2014 �. 14:49:20
* Purpose: Declaration of the class human
***********************************************************************/

#if !defined(__RGR_human_h)
#define __RGR_human_h

#include <string>

using std::string;

class human
{
public:
	human(int, string);
	virtual string getName() const = 0;			///< Get name
	
	int id;										///< Id of human
	string name;								///< Name of human

protected:
private:
	
};

#endif