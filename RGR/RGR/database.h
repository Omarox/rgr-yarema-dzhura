#include <vector>
#include <list>
#include <string>

namespace constants
{
	const int DAYS = 20;
	const int TICKS_PER_DAY = 24;
	const int TIME_PER_TICK = 30;
	const int MAX_HP = 1000;
	const int MAX_DAMAGE = 50;
	const int MAX_DESEASE_DAMAGE_EVOLUTION = 10;
	const int MAX_NUMBER_OF_DESEASES = 3;
	const int MAX_DOCTOR_TIME = 3;
	const int MAX_PATIENT_PER_TICK = 3;
	const int MAX_DEPARTMENT_PLACES = 200;
	const int OPENING_TIME = 8;
	const int CHANCE_OF_DESEASE_EVOLUTION = 50;
	const int CHANCE_TO_SET_WRONG_DIAGNOSE = 20;
	const int CHANCE_OF_PATIENT_APPEAR = 10;
	const int CHANCE_OF_CURE_DAMAGE = 60;
	const int CHANCE_OF_SUDDEN_DEATH = 1;
	const int CHANCE_OF_CLEVER_PATIENT = 2;
}

using namespace std;

struct cureAttributes
{
	int deseaseId;
	string name;
	int hp;

};

class database
{
public:
	~database();												///< Destructor
	static database& instance()									///< Get database
	{															
		static database theSingleInstance;						///< Create if does not exist
		return theSingleInstance;								///< Return it
	}			
	string getName() const;										///< Get random name
	/*!
	\param symptomId id of symptom to get its name
	*/
	string getSymptomName(const int) const;						///< Get symptom name
	/*!
	\param deseaseId id of desease to get its name
	*/
	string getDeseaseName(const int) const;						///< Get desease name
	int getSymptomAmount() const;								///< Amount of symptoms
	int getDeseaseAmount() const;								///< Amount of deseases
	/*!
	\param id id of cure to get its attributes
	*/
	cureAttributes* getCureAttributes(const int);				///< Get attributes of cure
	/*!
	\param desease id id of desease to search symptoms
	\param symptomsList list to write desease symptoms
	*/
	void getDeseaseSymptoms(const int, list<int>*) const;		///< Get sympoms of desease
																
private:														
	vector<string> symptomList;									///< List of symptoms
	vector<string> deseaseList;									///< List of deseases
	vector<string> nameList;									///< List of names
	vector<vector<int>> deseaseSymptoms;						///< List of symptoms of deseases
	vector<cureAttributes> cureList;							///< List of cure attributes

	database();													///< Constructor

};