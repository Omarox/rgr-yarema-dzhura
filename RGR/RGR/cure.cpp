/***********************************************************************
 * Module:  cure.cpp
 * Author:  Yarema & Dzhura
 * Modified: 19 ������� 2014 �. 14:41:19
 * Purpose: Implementation of the class cure
 ***********************************************************************/

#include <iostream>

#include "cure.h"
#include "database.h"

/// Default constructor
cure::cure() : influence(0, ""), deseaseId(0), fit(false)
{

}

/// Initialisation constructor
cure::cure(const int id) : influence(0, ""), fit(rand() % 2)
{
	cureAttributes data = *(database::instance().getCureAttributes(id));
	this->deseaseId = data.deseaseId;
	this->name = data.name;
	this->hp = data.hp;
	std::cout << "Cure was created." << endl;
}

/// Copy constructor
cure::cure(const cure& source) : influence(source.hp, source.name), deseaseId(source.deseaseId), fit(source.fit)
{

}

/// Destructor
cure::~cure()
{
	cout << "Cure was destroyed" << endl;
}

/// Heal patient
int cure::heal()
{
	return (this->hp)*(int)(fit);
}

/// Get cure name
string cure::getName() const
{
	return this->name;
}

bool cure::isFit() const
{
	return this->fit;
}

int cure::getDeseaseId()
{
	return this->deseaseId;
}