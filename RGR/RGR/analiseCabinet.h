/***********************************************************************
 * Module:  analiseCabinet.h
 * Author:  Yarema & Dzhura
 * Modified: 21 ������� 2014 �. 10:41:53
 * Purpose: Declaration of the class analiseCabinet
 ***********************************************************************/

#if !defined(__RGR_analiseCabinet_h)
#define __RGR_analiseCabinet_h

#include <queue>

#include "desease.h"
#include "PatientHandler.h"

using namespace std;

class patient;

class analiseCabinet : private patientHandler
{
public:
	analiseCabinet();							///< Default constructor
	/*!
	\param patientAtOnce patients that can be analised at the same time
	*/
	analiseCabinet(const int);					///< Initialisation constructor
	/*!
	\param source analise cabinet to copy from
	*/
	analiseCabinet(const analiseCabinet&);		///< Copy constructor
	~analiseCabinet();							///< Destructor
	/*!
	\sa checkDesease()
	*/
	void analise(void);							///< Analise desease of patient
	/*!
	\param myPatient a patient to add to cabinet queue
	*/
	void addPatient(patient* const);			///< Add patient to queue
private:
	int patientAtOnce;							///< Amount of patients analised at once
	int time;									///< Time per patient

	void checkDesease(desease*);				///< Checking deseases of patient
};

#endif