#include "database.h"
#include <string>
#include <list>
#include <iostream>
#include <fstream>

database::database()
{
	/// Read list of names from file
	ifstream nameFile("nameFile.txt");
	while (!nameFile.eof())
	{
		string name;
		nameFile >> name;
		this->nameList.push_back(name);
	}
	nameFile.close();

	/// Read list of sympoms from file
	ifstream symptomFile("symptomFile.txt");
	while (!symptomFile.eof())
	{
		string name;
		symptomFile >> name;
		this->symptomList.push_back(name);
	}
	symptomFile.close();

	/// Read list of deseases and list of desease sympoms from file
	ifstream deseaseFile("deseaseFile.txt");
	while (!deseaseFile.eof())
	{
		vector<int> data;
		string name;
		deseaseFile >> name;
		int id;
		do
		{
			deseaseFile >> id;
			/// End of desease description
			if (id != -1)
				data.push_back(id);
		} while (id != -1);
		deseaseSymptoms.push_back(data);
		this->deseaseList.push_back(name);
	}
	deseaseFile.close();

	/// Read list of cure attributes from file
	ifstream cureFile("cureFile.txt");
	while (!cureFile.eof())
	{
		cureAttributes attributes;
		cureFile >> attributes.deseaseId >> attributes.name >> attributes.hp;
		this->cureList.push_back(attributes);
	}
	cureFile.close();
}

/// Destructor
database::~database()
{
	std::cout << "Database was destroyed" << endl;
}

/// Get random name
string database::getName() const
{
	return nameList[rand() % nameList.size()];
}

/// Get amount of symptoms
int database::getSymptomAmount() const
{
	return symptomList.size();
}

/// Get amount of deseases
int database::getDeseaseAmount() const
{
	return deseaseList.size();
}

/// Get name of symptom using the id
string database::getSymptomName(const int symptomId) const
{
	return symptomList[symptomId];
}

/// Get name of desease using the id
string database::getDeseaseName(const int deseaseId) const
{
	return deseaseList[deseaseId];
}

/// Get attributes of cure using the id
cureAttributes* database::getCureAttributes(const int cureId)
{
	return &cureList[cureId];
}

void database::getDeseaseSymptoms(const int id, list<int>* symptoms) const
{
	for (int i = 0; i < deseaseSymptoms[id].size(); i++)
		symptoms->push_back(deseaseSymptoms[id][i]);
}