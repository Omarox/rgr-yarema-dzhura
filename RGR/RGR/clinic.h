/***********************************************************************
 * Module:  clinic.h
 * Author:  Yarema & Dzhura
 * Modified: 21 ������� 2014 �. 10:38:20
 * Purpose: Declaration of the class clinic
 ***********************************************************************/

#if !defined(__RGR_clinic_h)
#define __RGR_clinic_h

#include <list>
#include <vector>
#include "analiseCabinet.h"

using namespace std;

class patient;
class doctor;

class clinic
{
public:
	clinic();											///< Default constructor
	/*!
	\param doctorAmount amount of doctors in clinic
	\param patientAtOnce amount of patients analised at the same time in analise cabinet
	\param placesAmount amount of how many patients can be cured at clinic
	*/
	clinic(const int, const int, const int);			///< Initialisation constructor
	/*!
	\param source another clinic to copy from
	*/
	clinic(const clinic&);								///< Copy constructor
	~clinic();											///< Destructor
	/*!
	\param toDelete pointer to patient to delete
	*/
	void destroyPatient(patient* const);				///< Kill patient
	void addDead();										///< Count dead
	void addCured();									///< Count cured
	doctor* chooseFreeDoctor();							///< Choose free doctor
	analiseCabinet* getAnaliseCabinet();				///< Return adress of analise cabinet
	int getDead() const;								///< Get amount of dead patients
	int getCured() const;								///< Get amount of cured patients
	/*!
	\sa createPatient()
	*/
	void timer();										///< Sheduler of program
	friend ostream& operator << (ostream&, clinic&);
	void operator [] (int);

private:
	int doctorAmount;									///< Amount of doctors wotking at clinic
	vector<doctor * const> doctors;						///< Doctors themselves
	analiseCabinet analises;							///< Analise cabinet
	vector<patient*> patients;							///< Patients
	int cured;											///< Amount of dead patients
	int dead;											///< Amount of cured patients
	int departmentPlaces;								///< Amount of pleces in full-time department
	int departmetPatients;								///< Amount of patients in full-time department

	void createPatient();								///< Create patient
	void operator ++ ();								///< Add patient to clinic
};

#endif