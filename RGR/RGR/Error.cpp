#include <iostream>
#include "Error.h"

using std::cout;

Error::Error(void* errorPointer) : errorPointer(errorPointer)
{

}

void Error::print() const
{
	cout << "Max size reached at adress " << this->errorPointer << endl;
	system("pause");
}