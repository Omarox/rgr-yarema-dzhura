/***********************************************************************
 * Module:  doctor.cpp
 * Author:  Yarema & Dzhura
 * Modified: 21 ������� 2014 �. 10:33:13
 * Purpose: Implementation of the class doctor
 ***********************************************************************/

#include <iostream>
#include <windows.h>

#include "doctor.h"
#include "database.h"
#include "patient.h"
#include "PatientHandler.h"
#include "Error.h"

/// Set color of text message
void setColor1(int color = 7)
{
	HANDLE hcon = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hcon, color);
}

/// Default constructor
doctor::doctor() : human(0, 0), time(0), patientHandler(), ticksTilAction(0)
{

}

/// Initialisation constructor
doctor::doctor(const int id) : human(id, database::instance().getName()), patientHandler(), ticksTilAction(time), time(rand() % constants::MAX_DOCTOR_TIME + 1)
{
	std::cout << "Doctor  " << this->name << " was created." << endl;
}

/// Copy constructor
doctor::doctor(const doctor & source) : human(source.id, source.name), time(source.time), ticksTilAction(source.ticksTilAction)
{

}

/// Destructor
doctor::~doctor()
{
	cout << "Doctor was destroyed" << endl;
}

/// Send patient on analises
inline void doctor::sendOnAnalises()
{
	(this->patientQueue.front())->goOnAnalises();
}

/// Set diagnose to the patient if doctor knows it
void doctor::setDiagnose()
{
	/// If there are oatients to work with
	if (patientQueue.size())
	{
		ticksTilAction--;
		while ((patientQueue.front()->getHp() > 2 * (constants::MAX_HP)) || (patientQueue.front()->getHp() < (-2) * (constants::MAX_HP)))
		{
			patientQueue.pop();
			return;
		}
	}
	else
		return;

	setColor1(patientQueue.front()->getColor());
	/// If end of work
	if (ticksTilAction < 1)
	{
		/// Free patient
		if (patientQueue.front()->getHp() > constants::MAX_HP)
		{
			cout << "Patient " << patientQueue.front()->getName() << " is cured!" << endl;
			patientQueue.front()->goAway();
			patientQueue.pop();
			return;
		}
		else
		if ((patientQueue.front()->getHp() < constants::MAX_HP / 2) && (patientQueue.front()->isAtHome()) && patientQueue.front()->getHp() > 0)
			keepInClinic();

		/// Try to set diagnose
		cout << "Doctor " << this->name << " checked patient " << patientQueue.front()->getName() << endl;
		if ((patientQueue.front()->getCard()->getDiagnoses()->size()) != patientQueue.front()->getCard()->getDeseases()->size())
			if (((patientQueue.front()->getCard())->getDeseases())->size() == 1)
			{
				list<int>* symptomsOfDeseases = new list<int>;
				for (int i = 0; i < database::instance().getDeseaseAmount(); i++)
					if (symptomsOfDeseases[i] == (*(patientQueue.front()->getCard()->getSymptoms())))
					{
						patientQueue.front()->getCard()->setDesease(i);
						(patientQueue.front()->getCard())->setDiagnoses(i);
						break;
					}
				delete symptomsOfDeseases;
			}
			else
				sendOnAnalises();
		chooseCure();
		/// Free patient
		patientQueue.pop();
		ticksTilAction = time;
		setColor1();
	}
}

/// Choose cure for patient
void doctor::chooseCure()
{
	for (list<int>::iterator i = ((patientQueue.front()->getCard())->getDiagnoses())->begin(); i != ((patientQueue.front()->getCard())->getDiagnoses())->end(); i++)
	{
		int j = 0;
		while ((database::instance().getCureAttributes(j))->deseaseId != (*i))
			j++;
		patientQueue.front()->setCure(j);
	}
}

/// Add patient to queue
void doctor::addPatient(patient* const  myPatient)
{
	if (patientQueue.size() == 1000)
		throw Error(this);
	patientQueue.push(myPatient);
}

/// Get doctors name
string doctor::getName() const
{
	return this->name;
}

/// Get doctors queue size
int doctor::getQueueSize() const
{
	return this->patientQueue.size();
}

/// Is doctor busy
bool doctor::isBusy() const
{
	if (patientQueue.size() == 0) return false;
	else return true;
}

/// Keep in clinic
void doctor::keepInClinic()
{
	setColor1(patientQueue.front()->getColor());
	patientQueue.front()->setHome(false);
	cout << "Patient " << patientQueue.front()->getName() << " is now cured at clinic" << endl;
	setColor1();
}

void doctor::sendHome()
{

}

doctor::operator string() const
{
	return this->name;
}