/***********************************************************************
 * Module:  desease.cpp
 * Author:  Yarema & Dzhura
 * Modified: 21 ������� 2014 �. 10:29:48
 * Purpose: Implementation of the class desease
 ***********************************************************************/

#include "patient.h"
#include "desease.h"
#include "database.h"

#include <iostream>

/// Default constructor
desease::desease() : influence(rand() % constants::MAX_DAMAGE + 1, ""), id(-1)
{
	std::cout << "Desease was created." << endl;
}

/// Copy constructor
desease::desease(const desease & source) : influence(source.hp, source.name), id(source.id)
{

}

/// Destructor
desease::~desease()
{
	cout << "Desease was destriyed" << endl;
}

/// Damage patient
int desease::damage()
{
	return this->hp;
}

/// Get name of desease
string desease::getName() const
{
	return this->name;
}

void desease::Progress()
{
	this->hp += rand() % constants::MAX_DESEASE_DAMAGE_EVOLUTION + 1;
}

void desease::Regress()
{
	if( this->hp > constants::MAX_DESEASE_DAMAGE_EVOLUTION) 
		this->hp -= rand() % constants::MAX_DESEASE_DAMAGE_EVOLUTION + 1;
}

int desease::getId()
{
	return this->id;
}

void desease::setId(int newId)
{
	this->id = newId;
}

void desease::setName(string newName)
{
	this->name = newName;
}