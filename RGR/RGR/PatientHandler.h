#if !defined(__RGR_patientHandler_h)
#define __RGR_patientHandler_h

#include <queue>

#include "patient.h"

using std::string;

class patientHandler
{
public:
	patientHandler();
	virtual void addPatient(patient* const) = 0;				///< Add patient to queue
	
	queue<patient*> patientQueue;								///< Queue of patients

protected:
private:

};

#endif