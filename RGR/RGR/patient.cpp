/***********************************************************************
 * Module:  patient.cpp
 * Author:  Yarema & Dzhura
 * Modified: 19 ������ 2014 �. 14:49:20
 * Purpose: Implementation of the class patient
 ***********************************************************************/

#include "desease.h"
#include "cure.h"
#include "card.h"
#include "patient.h"
#include "clinic.h"
#include "database.h"
#include "doctor.h"
#include "Error.h"

#include <iostream>
#include <windows.h>

/// Set color of text message
void setColor(int color = 7)
{
	HANDLE hcon = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hcon, color);
}

/// Default constructor
patient::patient() : human(0, 0), hp(0), home(false), myCure(0), myCard(), myClinic(), color(0)
{
	
}

/// Initialisation constructor
patient::patient(const int id, clinic* const clinic) : human(id, database::instance().getName()), hp(rand() % (constants::MAX_HP / 2) + constants::MAX_HP / 3), home(true), myCure(), myCard(), myClinic(clinic), color(rand() % 15 + 1)
{
	setColor(this->color);
	cout << "patient " << this->name << " entered the clinic." << endl;
	setColor();
}

/// Copy constructor
patient::patient(const patient & source) : human(source.id, source.name), hp(source.hp), home(source.home), myCure(source.myCure), myCard(source.myCard)
{

}

/// Destructor
patient::~patient()
{
	setColor(this->color);
	cout << "Patient was destroyed." << endl;
	setColor();
}

/// Decrease hp of patient
void patient::decreaseHP()
{
	if (rand() % 100 / constants::CHANCE_OF_SUDDEN_DEATH)
	{
		int multiplier = 1;
		if (!home)
			multiplier++;
		int damaged = this->myCard.deseaseDamage();
		this->hp -= damaged / multiplier;
		cout << this->name << " is damaged on " << damaged / multiplier << " hp." << endl;
	}
	else
	{
		this->hp = 0;
		cout << "Suddenly ";
	}
}

/// Increase hp of patient
void patient::increaseHP()
{
	int cured = 0;
	///if (rand() % constants::CHANCE_OF_CURE_DAMAGE)
	///{
	for (list<cure*>::iterator i = myCure.begin(); i != myCure.end(); i++)
	{
		if ((*i)->isFit())
		{
			cured += (*i)->heal();
			if (!(rand() % 100 / constants::CHANCE_OF_DESEASE_EVOLUTION))
			{
				list<desease*>* patientDeseases = this->myCard.getDeseases();
				for (list<desease*>::iterator j = (*patientDeseases).begin(); j != (*patientDeseases).end(); j++)
					if ((*i)->getDeseaseId() == (*j)->getId())
					{
						(*j)->Regress();
						cout << "Curing of desease " << (*j)->getName() << " of patient " << this->getName() << " is good." << endl;
					}
			}
		}
		else
		{
			if (!(int)(rand() % 100 / constants::CHANCE_OF_CURE_DAMAGE))
			{
				cured -= rand() % constants::MAX_DAMAGE;
				cout << this->name << " taken damage from cure" << endl;
			}
			if (!(int)(rand() % 100 / constants::CHANCE_OF_DESEASE_EVOLUTION))
			{
				list<desease*>* patientDeseases = this->myCard.getDeseases();
				for (list<desease*>::iterator j = (*patientDeseases).begin(); j != (*patientDeseases).end(); j++)
				if ((*i)->getDeseaseId() == (*j)->getId())
				{
					(*j)->Progress();
					cout << "Curing of desease " << (*j)->getName() << " of patient " << this->getName() << " is bad." << endl;
				}
			}
			//if (rand() % 100 / constants::CHANCE_OF_CLEVER_PATIENT)
			//	goToDoctor();
		}
	}
	int multiplier = 1;
	if (!home)
		multiplier++;
	this->hp += cured * multiplier;
	cout << this->name << " is cured on " << cured * multiplier << " hp." << endl;
}

/// Go to doctor
void patient::goToDoctor()
{
	setColor(this->color);
	doctor* myDoctor = myClinic->chooseFreeDoctor();
	try
	{
		myDoctor->addPatient(this);
	}
	catch (Error error)
	{
		error.print();
	}
	cout << "Patient " << this->name << " is now in queue to doctor " << myDoctor->getName() << endl;
	setColor();
}

/// Go on analises
void patient::goOnAnalises()
{
	setColor(this->color);
	analiseCabinet* myAnaliseCabinet = myClinic->getAnaliseCabinet();
	try
	{
		myAnaliseCabinet->addPatient(this);
	}
	catch (Error error)
	{
		error.print();
	}
	cout << "Patient " << this->name << " is now in queue to analise cabinet." << endl;
	setColor();
}
            
/// Get name of patient
string patient::getName() const
{
	return this->name;
}

/// Get hp of patient
int patient::getHp() const
{
	return this->hp;
}

/// Get color of messages about patient 
int patient::getColor() const
{
	return this->color;
}

/// Set home or not treatment
void patient::setHome(const bool source)
{
	this->home = source;
}

/// Set cure
void patient::setCure(const int id)
{
	myCure.push_back(new cure(id));
}

/// Set cure
void patient::setCure(list<int> * cureIdList)
{
	for (list<int>::iterator i = cureIdList->begin(); i != cureIdList->end(); i++)
		myCure.push_back(new cure(*i));
}

/// Is patient curing at home or not
bool patient::isAtHome()
{
	return this->home;
}

/// Damaging and/or ealing patient
bool patient::changeHP()
{
	setColor(this->color);
	this->increaseHP();
	this->decreaseHP();
	if (this->hp <= 0)
	{
		cout << this->name << " died." << endl;
		this->myClinic->addDead();
		this->myClinic->destroyPatient(this);
		setColor();
		return true;
	}
	else
		cout << this->name << " now has " << this->hp << " hp." << endl;
	if ((this->hp > constants::MAX_HP) || ((this->hp < constants::MAX_HP / 2) && home))
		goToDoctor();
	setColor();
	return false;
}

card* patient::getCard()
{
	return &(this->myCard);
}

void patient::goAway()
{
	this->myClinic->addCured();
	this->myClinic->destroyPatient(this);
}

patient* patient::operator ->()
{
	return this;
}