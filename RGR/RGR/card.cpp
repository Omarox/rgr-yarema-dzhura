/***********************************************************************
  * Module:  card.cpp
  * Author:  Yarema & Dzhura
  * Modified: 21 ������ 2014 �. 10:41:23
  * Purpose: Implementation of the class card
  ***********************************************************************/
 
#include "card.h"
#include "desease.h"
#include "database.h"
#include <list>

#include "patient.h"

#include <iostream>

/// Default constructor
card::card() : temperature(rand() % 7 + 34.0)
{
	/// Creating desease list
	int n = rand() % 3 + 1;
	for (int i = 0; i < n; i++)
		illness.push_back(new desease);
	bool isSymptom;
	/// Creating symptom list
	for (int i = 0; i < database::instance().getSymptomAmount(); i++)
	{
		isSymptom = (bool)(rand() % 2);
		this->symptoms.push_back(isSymptom);
	}
	std::cout << "Card was created" << endl;
}

/// Copy constructor
card::card(const card& source) : temperature(source.temperature)
{
	
}

/// Destructor
card::~card()
{
	for (int i = 0; i < illness.size(); i++)
		illness.pop_back();
	cout << "Card was destroyed" << endl;
}

/// Summ up damage of deseases
int card::deseaseDamage()
{
	int sumHP = 0;
	for (list<desease*>::iterator i = illness.begin(); i != illness.end(); i++)
		sumHP += (*i)->damage();
	return sumHP;
}

/// Get symptom list
list<int>* card::getSymptoms()
{
	return &(this->symptoms);
}

/// Get diagnose list
list<int>* card::getDiagnoses()
{
	return &(this->diagnoses);
}

///Get wrong diagnoses list
list<int>* card::getWrongDiagnoses()
{
	return &(this->wrongDiagnoses);
}

/// Get deseases
list<desease*>* card::getDeseases()
{
	return &(this->illness);
}

/// Set diagnoses
void card::setDiagnoses(list<int>* const diagnoseList)
{
	this->diagnoses = *diagnoseList;
}

/// Set diagnos
void card::setDiagnoses(const int diagnoseId)
{
	this->diagnoses.push_back(diagnoseId);
}

/// Set wrong diagnoses
void card::setWrongDiagnoses(list<int>* const wrongDiagnoseList)
{
	this->wrongDiagnoses = *wrongDiagnoseList;
}

/// Set desease info
void card::setDesease(int deseaseId)
{
	for (list<desease*>::iterator i = illness.begin(); i != illness.end(); i++)
	if ((*i)->getId() < 0)
	{
		(*i)->setId(deseaseId);
		(*i)->setName(database::instance().getDeseaseName(deseaseId));
		return;
	}
}