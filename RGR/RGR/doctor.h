/***********************************************************************
 * Module:  doctor.h
 * Author:  Yarema & Dzhura
 * Modified: 21 ������� 2014 �. 10:33:13
 * Purpose: Declaration of the class doctor
 ***********************************************************************/

#if !defined(__RGR_doctor_h)
#define __RGR_doctor_h

#include <string>
#include <queue>
#include "Human.h"
#include "PatientHandler.h"

using namespace std;

class patient;

class doctor : private human, private patientHandler
{
public:
	doctor();								///< Default constructor
	/*!
	\param id id of doctor
	*/
	doctor(const int);						///< Initialisation constructor
	/*!
	\param source another doctor to copy from
	*/
	doctor(const doctor &);					///< Copy constructor
	~doctor();								///< Destructor
	/*!
	\param myPatient patient to add to queue
	*/
	void addPatient(patient* const);		///< Add patient to queue
	//! Set diagnose to patient if doctor know the desease
	void setDiagnose();						///< Set diagnose to patient if doctor know the desease
	//! Get doctors name
	virtual string getName() const;			///< Get doctors name
	//! Get size of queue to doctor
	int getQueueSize() const;				///< Get size of queue to doctor
	//! Is doctor busy
	bool isBusy() const;					///< Is doctor busy
	operator string() const;

protected:
private:
	int time;								///< Time per patient
	int ticksTilAction;						///< periods of time till doctor makes his task
	
	inline void sendOnAnalises();			///< Send patient on analises
	void chooseCure();						///< Choose cure for patient
	void keepInClinic();					///< Keep petient in clinic
	void sendHome();						///< Send patient to cure at home
};

#endif