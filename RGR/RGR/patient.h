/***********************************************************************
 * Module:  patient.h
 * Author:  Yarema & Dzhura
 * Modified: 19 ������� 2014 �. 14:49:20
 * Purpose: Declaration of the class patient
 ***********************************************************************/

#if !defined(__RGR_patient_h)
#define __RGR_patient_h

#include "card.h"
#include "Human.h"

#include <list>

class clinic;
class cure;
class card;

class patient : private human
{
public:
	patient();									///< Default constructor
	/*!
	\param id id of patient
	\param myClinic clinic of patient
	*/
	patient(const int, clinic* const);			///< Initialisation constructor
	/*!
	\param source another patient to copy from
	*/
	patient(const patient &);					///< Copy constructor
	~patient();									///< Destructor
	void goToDoctor();							///< Go to doctor
	void goOnAnalises();						///< Go on analises
	virtual string getName() const;				///< Get name
	int getHp() const;							///< Get health
	int getColor() const;						///< Get id of color
	/*!
	\param home true if send to home
	*/
	void setHome(bool);							///< Set home or full-time department cure
	/*!
	\param cureIdList list of cure id
	*/
	void setCure(list<int>*);					///< Set cure
	bool isAtHome();							///< Is at home or full-time department cure
	/*!
	\sa increaseHP(), decreaseHP().
	*/
	bool changeHP();
	/*!
	\param cureId id of cure to set
	*/
	void setCure(const int);
	card* getCard();
	void goAway();
	patient* operator ->();

protected:
private:
	int color;									///< Id of color of text messages about patient
	///<int id;									///< Id of patient
	int hp;										///< Current hp of patient
	///<string name;								///< Name of patient
	bool home;									///< Cures at home or full-time department
	clinic* myClinic;							///< Clinic of patient
	list<cure*> myCure;							///< Cure of patient
	card myCard;								///< Card of patient

	void decreaseHP();							///< Decrease hp of patient
	void increaseHP();							///< Increase hp of patient
};

#endif