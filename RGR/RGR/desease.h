/***********************************************************************
 * Module:  desease.h
 * Author:  Yarema & Dzhura
 * Modified: 21 ������� 2014 �. 10:29:48
 * Purpose: Declaration of the class desease
 ***********************************************************************/

#if !defined(__RGR_desease_h)
#define __RGR_desease_h

#include <string>

#include "Influence.h"

using namespace std;

class patient;

class desease : private influence
{
public:
	desease();						///< Default constructor
	/*!
	\param source desease to copy from
	*/
	desease(const desease &);		///< Initialisation constructor
	~desease();						///< Destructor
	int damage();					///< Damage patient
	string getName() const;			///< Get name of desease
	void Progress();				///< Desease progress
	void Regress();					///< Desease regress
	int getId();					///< Get desease id
	/*!
	\param deseaseId id to set
	*/
	void setId(int);				///< Set desease id
	/*!
	\param deseaseName name to attache to desease
	*/
	void setName(string);			///< Set name of desease

protected:
private:
	int id;							///< Id of desease


};

#endif