/***********************************************************************
 * Module:  cure.h
 * Author:  Yarema & Dzhura
 * Modified: 19 ������� 2014 �. 14:41:19
 * Purpose: Declaration of the class cure
 ***********************************************************************/

#if !defined(__RGR_cure_h)
#define __RGR_cure_h

#include <string>

#include "Influence.h"

using namespace std;

class cure : private influence
{
public:
	cure();						///< Default constructor
	/*!
	\param id id of cure in database
	*/
	cure(const int);			///< Initialisation constructor
	/*!
	\param source cure to copy from
	*/
	cure(const cure&);			///< Copy constructor
	~cure();					///< Destructor
	int heal();					///< Heal patient
	string getName() const;		///< Get name of cure
	bool isFit() const;
	int getDeseaseId();			///< Get id of desease cured by this

protected:
private:
	int deseaseId;				///< Id of desease
	bool fit;					///< Fits cure to desease or not
};

#endif