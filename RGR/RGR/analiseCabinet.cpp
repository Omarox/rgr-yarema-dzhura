/***********************************************************************
 * Module:  analiseCabinet.cpp
 * Author:  Yarema & Dzhura
 * Modified: 21 ������� 2014 �. 10:41:53
 * Purpose: Implementation of the class analiseCabinet
 ***********************************************************************/

#include <iostream>
#include <string>

#include "analiseCabinet.h"
#include "patient.h"
#include "database.h"
#include "Error.h"

/// Default constructor
analiseCabinet::analiseCabinet() : patientHandler(), patientAtOnce(0), time(0)
{

}

/// Initialisation constructor
analiseCabinet::analiseCabinet(const int patientAtOnce) : patientHandler(), patientAtOnce(patientAtOnce), time((int)(rand() % 3 + 1))
{
	std::cout << "Analise Cabinet was created." << endl;
}

/// Copy constructor
analiseCabinet::analiseCabinet(const analiseCabinet& source) : patientHandler(), time(source.time), patientAtOnce(source.patientAtOnce)
{

}

/// Destructor
analiseCabinet::~analiseCabinet()
{
	cout << "Analise cabinet was destroyed" << endl;
}

/// Analise the desease
void analiseCabinet::analise()
{
	if (patientQueue.size() > 0)
		while ((patientQueue.front()->getHp() > 2 * (constants::MAX_HP)) || (patientQueue.front()->getHp() < (-2) * (constants::MAX_HP)))
		{
			patientQueue.pop();
			return;
		}
	list<int> diagnoses;
	/// How many patients is analise cabinet going to check
	int toCheck = patientQueue.size();
	if (toCheck > patientAtOnce)
		toCheck = patientAtOnce;

	/// Checking each patient
	for (int i = 0; i < toCheck; i++)
	{
		list<int>* symptomsToCheck = patientQueue.front()->getCard()->getSymptoms();
		symptomsToCheck->sort();

		/// Checking current patient
		for (int k = 0; k < patientQueue.front()->getCard()->getDeseases()->size(); k++)
		{
			int maxFit = 0;
			int deseaseId = 0;
			
			/// Choosing the diagnose that fits symptoms the most
			for (int j = 0; j < database::instance().getDeseaseAmount(); j++)
			{
				list<int>* deseaseSymptoms = new list<int>;
				database::instance().getDeseaseSymptoms(j, deseaseSymptoms);
				deseaseSymptoms->sort();
				list<int>* summary = new list<int>(*symptomsToCheck);
				summary->merge(*deseaseSymptoms);

				/// Counting how many symptoms are the same
				int amount = summary->size();
				summary->unique();

				/// Remembering the diagnose
				if (amount - summary->size() > maxFit)
				{
					bool isNew = true;
					for (list<int>::iterator i = diagnoses.begin(); i != diagnoses.end(); i++)
					if ((*i) == j)
						isNew = false;

					/// Check if not repeated
					if (!diagnoses.size() || isNew)
					{
						maxFit = amount - summary->size();
						deseaseId = j;
					}
				}
			}
			/// Add founded diagnose
			diagnoses.push_back(deseaseId);
			patientQueue.front()->getCard()->setDesease(deseaseId);
		}

		/// Set diagnoses and send patient to doctor
		patientQueue.front()->getCard()->setDiagnoses(&diagnoses);
		patientQueue.front()->goToDoctor();
		patientQueue.pop();
	}
}

/// Add patient to queue
void analiseCabinet::addPatient(patient* const myPatient)
{
	if (patientQueue.size() == 1000)
		throw Error(this);
	patientQueue.push(myPatient);
}