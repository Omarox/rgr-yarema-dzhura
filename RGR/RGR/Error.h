/***********************************************************************
* Module:  doctor.h
* Author:  Yarema & Dzhura
* Modified: 21 ������� 2014 �. 10:33:13
* Purpose: Declaration of the class doctor
***********************************************************************/

#if !defined(__RGR_Error_h)
#define __RGR_Error_h

#include <iostream>

using namespace std;

class patient;

class Error
{
public:
	Error(void*);
	void print() const;

protected:
private:
	void *errorPointer;
};

#endif