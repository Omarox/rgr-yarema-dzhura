/***********************************************************************
 * Module:  card.h
 * Author:  Yarema & Dzhura
 * Modified: 21 ������� 2014 �. 10:41:23
 * Purpose: Declaration of the class card
 ***********************************************************************/

#if !defined(__RGR_card_h)
#define __RGR_card_h

#include <list>

using namespace std;

class desease;

class card
{
public:
	card();											///< Default constructor
	/*!
	\param source card to copy from
	*/
	card(const card&);								///< Initialisation constructor
	~card();										///< Destructor
	
	list<int>* getSymptoms();						///< Get symptoms
	list<int>* getDiagnoses();						///< Get diagnoses
	list<int>* getWrongDiagnoses();					///< Get wrong diagnoses
	/*!
	\param diagnoseId id of desease to set into diagnose list
	*/
	void setDiagnoses(const int);					///< Set diagnose
	/*!	
	\param diagnoseList a list of desease id
	*/
	void setDiagnoses(list<int>* const);			///< Set diagnoses
	/*!
	\param wrongDiagnoseList list of wrong diagnoses
	*/
	void setWrongDiagnoses(list<int>* const);		///< Set wrong diagnoses
	int deseaseDamage();							///< Summ up deseases damage
	list<desease*>* getDeseases();					///< Get list of deseases
	/*!
	\param deseaseId id of desease to set info from
	*/
	void setDesease(int);							///< Set desease info
	
private:
	float temperature;								///< Temperature of patient
	list<int> symptoms;								///< Symptoms of patient
	list<int> diagnoses;							///< Diagnoses of patient
	list<int> wrongDiagnoses;						///< Wrong diagnoses
	list<desease*> illness;							///< Deseases
};

#endif