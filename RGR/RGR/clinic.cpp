/***********************************************************************
 * Module:  clinic.cpp
 * Author:  Yarema & Dzhura
 * Modified: 21 ������� 2014 �. 10:38:20
 * Purpose: Implementation of the class clinic
 ***********************************************************************/

#include <iostream>
#include <iomanip>

#include "patient.h"
#include "database.h"
#include "analiseCabinet.h"
#include "doctor.h"
#include "clinic.h"
#include "Error.h"

using namespace constants;

/// Default constructor
clinic::clinic() : doctors(), analises(0), departmentPlaces(0)
{

}

/// Initialisation constructor
clinic::clinic(const int placesAmount, const int doctorAmount, const int analisesAmount) : departmentPlaces(placesAmount), doctors(), analises(analisesAmount)
{
	/// Create doctors
	for (int i = 0; i < doctorAmount; i++)
		doctors.push_back(new doctor(i));
	std::cout << "Clinic was created." << endl;
}

/// Copy constructor
clinic::clinic(const clinic& source) : departmentPlaces(source.departmentPlaces), doctors(source.doctors), analises(source.analises)
{

}

/// Destructor
clinic::~clinic()
{
	for (int i = 0; i < doctors.size(); i++)
		doctors.pop_back();
	for (int i = 0; i < patients.size(); i++)
		patients.pop_back();
	cout << "Clinic was destroyed" << endl;
}

/// Create patient
void clinic::createPatient()
{
	if (patients.size() == patients.max_size())
		throw Error(this);
	patients.push_back(new patient(patients.size(), this));
	patients.back()->goToDoctor();
}

void clinic::operator ++ ()
{
	if ((int)patients.size() == 1000)
		throw Error(this);
	patients.push_back(new patient(patients.size(), this));
	patients.back()->goToDoctor();
		
}

/// Kill patient
void clinic::destroyPatient(patient* const useless)
{
	for (vector<patient*>::iterator i = patients.begin(); i != patients.end(); i++)
	if ((*i) == useless)
	{
		delete (*i);
		patients.erase(i);
		return;
	}
}

/// Find doctor with min queue
doctor* clinic::chooseFreeDoctor()
{
	int min = doctors[0]->getQueueSize();
	doctor* doctorWithMinQueue = doctors[0];
	/// Search of the most free doctor
	for (int i = 0; i < doctors.size(); i++)
	{
		/// If there is free doctor
		if (!(doctors[i]->isBusy()))
			return doctors[i];
		else
		if (doctors[i]->getQueueSize() < min)
		{
			min = doctors[i]->getQueueSize();
			doctorWithMinQueue = doctors[i];
		}
	}
	return doctorWithMinQueue;
}

/// Get adress of analise cabinet
analiseCabinet* clinic::getAnaliseCabinet()
{
	return &analises;
}

/// Add dead patient
void clinic::addDead()
{
	this->dead++;
}

/// Add cured patient
void clinic::addCured()
{
	this->cured++;
}

///Get amount of dead patients
int clinic::getDead() const
{
	return this->dead;
}

///Get amount of cured patients
int clinic::getCured() const
{
	return this->cured;
}

void clinic::timer()
{
	int currentTime = 0;
	int currentDay = 0;
	int patientCreated = 0;

	cout << "===================== DAY: " << ++currentDay << " ==================================" << endl;

	while (currentDay <= DAYS)
	{
		int hour = currentTime*(TIME_PER_TICK) / 60 % 24;
		cout << "Current time : " << hour + OPENING_TIME << " : " << currentTime*(TIME_PER_TICK) - hour * 60;
		if (currentTime*(TIME_PER_TICK)-hour * 60 == 0)
			cout << "0";
		cout << endl;

		/// Analise cabinet work
		analises.analise();

		/// Doctors work
		for (vector<doctor*>::iterator i = doctors.begin(); i != doctors.end(); i++)
			(*i)->setDiagnose();

		/// Creating new patient
		for (int i = 0; i < rand() % constants::MAX_PATIENT_PER_TICK + 1; i++)
		if (!(rand() % constants::CHANCE_OF_PATIENT_APPEAR))
		{
			try
			{
				(*this)++;
			}
			catch (Error error)
			{
				error.print();
			}
			patientCreated++;
		}

		/// Change day
		if (!(currentTime % TICKS_PER_DAY) && currentTime > 0)
		{
			/// Cure and damage patients
			for (int i = 0; i < patients.size(); i++)
			if (patients[i]->changeHP())
				i--;
			if (++currentDay <= DAYS) 
				cout << "===================== DAY: " << currentDay << " ==================================" << endl;
			currentTime = 0;
			system("pause");
		}
		else
			currentTime++;

	}
	cout << *this;
	cout << left << "|     Patients spawned : " << setw(6) << patientCreated << "|" << endl;
	cout << "================================" << endl;
}

ostream& operator << (ostream& os, clinic& myClinic)
{
	os << "================================" << endl;
	os << "|Result of clinic work" << setw(10) << "|" << endl;
	os << left << "|     Dead: " << setw(19) << myClinic.dead << "|" << endl;
	os << left << "|     Cured: " << setw(18) << myClinic.cured << "|" << endl;
	return os;
}

void clinic::operator [] (int i)
{
	patients.erase(patients.begin() + i - 1);
}