/***********************************************************************
* Module:  cure.h
* Author:  Yarema & Dzhura
* Modified: 19 ������� 2014 �. 14:41:19
* Purpose: Declaration of the class cure
***********************************************************************/

#if !defined(__RGR_Influence_h)
#define __RGR_Influence_h

#include <string>

using std::string;

class influence
{
public:
	influence(int, string);
	string getName() const;		///< Get name of influnce
	string name;				///< Name of influnce
	int hp;						///< Amount of hp influence increase/decrease

protected:
private:
	
};

#endif