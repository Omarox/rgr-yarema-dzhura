#include <string>
#include <iostream>
#include "car.h"

using namespace carSpace;

using namespace std;

	car::car(const string& firm, const std::string& mark, float petrolV, float petrolPerKilo)
	{
		producer = firm;
		model = mark;
		petrol = petrolV;
		petrolPerHundredKilometr = petrolPerKilo;
	}

	car::~car()
	{
		cout << "Car " << producer << " " << model << " is destroyed" << endl;
	}

	void car::print()
	{
		cout << "Producer : " << producer << endl;
		cout << "Model : " << model << endl;
		cout << "Petrol : " << petrol << endl;
		cout << "Petrol per 100 kilometers : " << petrolPerHundredKilometr << endl;
	}

	float car::litrage(float length)
	{
		if (length * petrolPerHundredKilometr / 100 > petrol)
			cout << "You need to fill your petrol " << (int)(length * petrolPerHundredKilometr / 100 / petrol) << " times" << endl;
		return length * petrolPerHundredKilometr / 100;
	}

	void car::operator<< (ostream &os)
	{
		os << "Producer : " << producer << endl;
		os << "Model : " << model << endl;
		os << "Petrol : " << petrol << endl;
		os << "Petrol per 100 kilometers : " << petrolPerHundredKilometr << endl;
	}

	ostream& operator<< (ostream& os, const car& c)
	{
		os << "Producer : " << c.producer << endl;
		os << "Model : " << c.model << endl;
		os << "Petrol : " << c.petrol << endl;
		os << "Petrol per 100 kilometers : " << c.petrolPerHundredKilometr << endl;
		return os;
	}

	istream& operator>> (istream& is, car& c)
	{
		is >> c.producer;
		is >> c.model;
		is >> c.petrol;
		is >> c.petrolPerHundredKilometr;
		return is;
	}
