#include <string>

using namespace std;

class car
{
public:
	car(const string&, const string&, float, float);
	~car();
	void print();
	void operator<< (ostream&);
	friend ostream& operator<< (ostream&, const car&);
	friend istream& operator>> (istream&, car&);
	float litrage(float);
private:
	string producer;
	string model;
	float petrol;
	float petrolPerHundredKilometr;
};

namespace carSpace
{
	void car::print();
}